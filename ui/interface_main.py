import tkinter as tk


class Interface(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.var1_entry_frame1 = tk.StringVar()
        self.var2_entry_frame1 = tk.StringVar()

        self.frame2 = tk.Frame(self.master)

        self.create_widget()

    def create_widget(self):
        frame1 = tk.Frame(self.master)
        frame1.grid(row=0, column=0, padx=1, pady=1)

        label1_frame1 = tk.Label(frame1, text='Distância em m ')
        label1_frame1.grid(row=0, column=0, padx=1)
        label2_frame1 = tk.Label(frame1, text='Tempo em s ')
        label2_frame1.grid(row=1, column=0, padx=1)

        entry_frame1 = tk.Entry(frame1, textvariable=self.var1_entry_frame1)
        entry_frame1.grid(row=0, column=1)
        entry_frame1 = tk.Entry(frame1, textvariable=self.var2_entry_frame1)
        entry_frame1.grid(row=1, column=1)

        button_frame1 = tk.Button(frame1, text='Calcular\nVelocidade', command=self.calc_velocidade)
        button_frame1.grid(row=2, column=1)

    def calc_velocidade(self):
        self.frame2.grid(row=1, column=0)
        try:
            d = float(self.var1_entry_frame1.get())
            t = float(self.var2_entry_frame1.get())
            v = d/t
            text = f'{v:.2f} m/s'
            label_frame2 = tk.Label(self.frame2, text=text, width=30)
        except Exception:
            label_frame2 = tk.Label(self.frame2, text='dados inválidos', width=30)
        finally:
            label_frame2.grid(row=0, column=0, padx=10)
