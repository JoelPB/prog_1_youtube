import tkinter as tk
from ui.interface_main import Interface


def start():
    root = tk.Tk()
    root.title('Física aplicada')
    root.geometry('300x200')

    app = Interface(master=root)
    app.mainloop()


if __name__ == '__main__':
    start()
